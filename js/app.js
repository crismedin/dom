
/*nuestra base de datos simulada*/
var arrURL = ["https://s3.amazonaws.com/mercado-ideas/wp-content/uploads/sites/2/2017/12/09003940/iphone-xs.jpg",
"https://noticiasibo.files.wordpress.com/2020/06/huawei-p40-lite.jpg",
"https://s3.amazonaws.com/imagenes-sellers-mercado-ripley/2020/02/27135722/SAMSUNG-GALAXY-A51-BLANCO-1.jpg"];
var marcas = ["IPhone","Huawei","Samsung"];
var descripcion = ["El nuevo IPhone que se parece a los demas pero es nuevo.","El nuevo huawei con nuestro propio sistema operativo",
"Este es el nuevo samsung sin bateria explosiva"];
var precios = [1000000, 700000, 750000];
var cant = [0,0,0];

document.addEventListener("DOMContentLoaded", pintarImg());

function pintarImg(){
	/*crenado las cards*/
	const template = document.querySelector('#template').content;/*NO OLVIDAR EL CONTENT*/
	const fragment = document.createDocumentFragment();
	const contenedorTelefonos = document.querySelector("#contenedor-telefonos");

	for (let i = 0; i < arrURL.length; i++) {
		template.querySelector('img').setAttribute('src', arrURL[i]);
		template.querySelector('.card-title').textContent = marcas[i]+' '+precios[i]+' $';
		template.querySelector('.card-text').textContent = descripcion[i];
		template.querySelector('.compra-btn').setAttribute('id', i);
		/*debemos clonar el template*/
		const clone = template.cloneNode(true);
		fragment.appendChild(clone);
	}

	contenedorTelefonos.appendChild(fragment);
	compra();
}

function compra(){
	/*Obtenemos los botones de compra y creamos el evento de compra*/
	const botones = document.querySelectorAll('.compra-btn');
	
	botones.forEach( function(elemento,index) {
			elemento.addEventListener('click', ()=>{
		    cant[index]++;
		    pintarCarrito();
		})
	});

}


/*pintamos el carrito */

function pintarCarrito(){
	const contenedorFilas = document.querySelector('#contenedor-filas');
	const filaTemplate = document.querySelector('#filas').content;/*NO OLVIDAR EL CONTENT*/
	const fragment = document.createDocumentFragment();

	contenedorFilas.innerHTML = '';
	let total = 0;
		/*sumando total y repintando carrito*/
			for(let j = 0; j < marcas.length; j++){
					if(cant[j] != 0){
						
						const totalFila = cant[j]*precios[j];

						filaTemplate.querySelector('.producto').textContent = marcas[j];
						filaTemplate.querySelector('.cantidad').textContent = cant[j];
						filaTemplate.querySelector('.total').textContent = totalFila+' $';
						filaTemplate.querySelector('.btn-mas-menos').setAttribute('id', j);

						const clone = filaTemplate.cloneNode(true);

						total += totalFila; 

						fragment.appendChild(clone);
						
				}
			}
			contenedorFilas.appendChild(fragment);
			document.querySelector('.total-pagar').textContent = total+' $';
			accionBotones();
			
}
/*accion de botones mas y menos*/
 
function accionBotones(){
	/*Obtenemos los botones y agregamos la accion*/
	const mas = document.querySelectorAll('.mas');
	const menos = document.querySelectorAll('.menos');

	mas.forEach(function(elemento){
		elemento.addEventListener('click', ()=>{
			cant[event.target.parentNode.id]++;
			pintarCarrito();
		})
	})

	menos.forEach(function(elemento){

		elemento.addEventListener('click', ()=>{
			cant[event.target.parentNode.id]--;
			pintarCarrito();
		})
	})
}

function terminar(){
	/*terminamos la compra*/
	if(document.querySelector('#contenedor-filas').childNodes.length == 0)
		alert('Primero agrega elementos al carrito!')
	else{
		cant.forEach( function(element, index) {
			cant[index] = 0;
		});
		pintarCarrito();
		alert("Disfruta tu compra! ;)");
	}
}




